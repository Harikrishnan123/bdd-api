package frameworkUtils;

import com.fasterxml.jackson.databind.JsonNode;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertNotNull;

public class ApiStepDefinitation {
    JsonNode jsonNode = null;
    JsonPath jsonPath = null;
    private static final Logger logger = Logger.getLogger(ApiStepDefinitation.class.getName());
    public static Response response = null;
    public static String getuserId_Api;


    @When("^I execute the get api with url \"([^\"]*)\"$")
    public void executeGraphQLApi( String URL){
        response = getApiCall(URL);

        if(response.statusCode()==200) {
            logger.info("Received status code .." + response.statusCode());
        }
        else {
            logger.error("Error in api..." + response.statusCode());
            logger.error("Error in api..." + response.getBody().toString());
        }
    }

    @When("^I validate the search username \"([^\"]*)\" result$")
    public void validateSearchResult( String Username){
        List<String> jsonResponse = response.jsonPath().getList("$");
        List<String> getId = response.jsonPath().getList("id");
        List<String> getName = response.jsonPath().getList("name");
        List<String> getEmail = response.jsonPath().getList("email");
        List<String> getPhone = response.jsonPath().getList("phone");
        List<String> getwebsite = response.jsonPath().getList("website");
        List<String> getUsername = response.jsonPath().getList("username");
        logger.info("Received Json Response of search username .." + jsonResponse);
        if(jsonResponse.size()!=0)
        {
            logger.info("Received Json Response of search username with value" + jsonResponse);
            for(int i =0;i<=jsonResponse.size()-1;i++)
            {
                logger.info(getUsername.get(i)+" checking username is match with the given username " + Username);
                if(getUsername.get(i).equalsIgnoreCase(Username)) {
                    getuserId_Api = String.valueOf(getId.get(i));
                    assertNotNull(getUsername.get(i));
                    assertNotNull(getName.get(i));
                    assertNotNull(getEmail.get(i));
                    assertNotNull(getPhone.get(i));
                    assertNotNull(getwebsite.get(i));
                    logger.info("Username: "+getUsername.get(i)+"\n Name:  " + getName.get(i) + "\nEmail: "+getEmail +"\n Phone: "+getPhone.get(i)+"\n website:"+getwebsite);
                    break;
                }
                else
                {
                    logger.error("Id is not matching");
                }
            }
        }
        else
        {
            logger.error("Size is 0");
        }
    }

    @When("^I validate the post posted by the user \"([^\"]*)\" result$")
    public void validatePostResult(String Username){
        List<String> jsonResponsePost = response.jsonPath().getList("$");
        List<Integer>  getIdList= response.jsonPath().getList("id");
        List<Integer>  getuserId= response.jsonPath().getList("userId");
        List<String> gettitle = response.jsonPath().getList("title");
        List<String> getbody = response.jsonPath().getList("body");

        if(jsonResponsePost.size()!=0)
        {logger.info("Posted result is "+jsonResponsePost );
            for(int j =0;j<=jsonResponsePost.size()-1;j++)
            {
                if(getIdList.get(j)==Integer.parseInt(getuserId_Api)) {
                    assertNotNull(getuserId.get(j));
                    assertNotNull(gettitle.get(j));
                    assertNotNull(getbody.get(j));
                    logger.info("UserId: "+getuserId.get(j)+"\n title:  " + gettitle.get(j) + "\nbody: "+getbody );
                    break;
                }
                else
                 {
                    logger.error("Id is not matching");
                }
            }
        }
        else
        {
            logger.error("Size is 0");
        }
    }

    @When("^I validate the comments and validate the emails is correct or not$")
    public void ValidateEmail(){
        List<String> jsonResponsePost = response.jsonPath().getList("$");
        List<Integer>  getCommetnId= response.jsonPath().getList("id");
        List<Integer>  getCommetnPostID= response.jsonPath().getList("postId");
        List<String>  getCommetnname= response.jsonPath().getList("name");
        List<String>  getCommetnemail= response.jsonPath().getList("email");
        List<String>  getCommetnbody= response.jsonPath().getList("body");
        if(jsonResponsePost.size()!=0)
        {
            for(int k =0;k<=jsonResponsePost.size()-1;k++)
            {
                assertNotNull(getCommetnId.get(k));
                assertNotNull(getCommetnPostID.get(k));
                assertNotNull(getCommetnname.get(k));
                assertNotNull(getCommetnemail.get(k));
                assertNotNull(getCommetnbody.get(k));
                emailCheck(getCommetnemail.get(k));
                logger.info("commentId: "+getCommetnId.get(k)+"\n postid:  " + getCommetnPostID.get(k) + "\ncommentname: "+getCommetnname.get(k)+
                "\nbody:"+getCommetnbody+ "\n email"+getCommetnemail.get(k));
            }
        }
        else
        {
            logger.error("Size is 0");
        }
    }

    @When("^I validate the comments and validate the emails for particular id$")
    public void ValidateEmailParticular(){
        List<String> jsonResponsePost = response.jsonPath().getList("$");
        List<Integer>  getCommetnId= response.jsonPath().getList("id");
        List<Integer>  getCommetnPostID= response.jsonPath().getList("postId");
        List<String>  getCommetnname= response.jsonPath().getList("name");
        List<String>  getCommetnemail= response.jsonPath().getList("email");
        List<String>  getCommetnbody= response.jsonPath().getList("body");
        if(jsonResponsePost.size()!=0)
        {
            for(int k =0;k<=jsonResponsePost.size()-1;k++)
            {
                if(getCommetnId.get(k)==Integer.parseInt(getuserId_Api)) {
                    assertNotNull(getCommetnId.get(k));
                    assertNotNull(getCommetnPostID.get(k));
                    assertNotNull(getCommetnname.get(k));
                    assertNotNull(getCommetnemail.get(k));
                    assertNotNull(getCommetnbody.get(k));
                    emailCheck(getCommetnemail.get(k));
                    logger.info("commentId: " + getCommetnId.get(k) + "\n postid:  " + getCommetnPostID.get(k) + "\ncommentname: " + getCommetnname.get(k) +
                            "\nbody:" + getCommetnbody + "\n email" + getCommetnemail.get(k));
                    break;
                }
                else
                {
                    logger.error("Id is not matching");
                }
            }
        }
        else
        {
            logger.error("Size is 0");
        }
    }


    public static Response getApiCall(String endpoint) {
        Log.startLog("Entered the endpoint url as "+endpoint);
        RestAssured.defaultParser = Parser.JSON;
        return
                given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
                        when().get(endpoint).
                        then().contentType(ContentType.JSON).extract().response();
    }

    public static void emailCheck(String emailVerify)
    {
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(emailVerify);
        logger.info(emailVerify +" : "+ matcher.matches()+"\n");

    }
}
