@Interview
Feature: Interview preparation

  Scenario Outline: Search the user with with username <UserName>
    Then I execute the get api with url "<URL>"
    Then I validate the search username "<UserName>" result
    Examples:
      |UserName   |URL|
      |Delphine   |https://jsonplaceholder.typicode.com/users|

  @SearchPostUserName
  Scenario Outline: Search the user with with username <UserName>
    Then I execute the get api with url "<URL>"
    Then I validate the post posted by the user "<UserName>" result
    Examples:
      |UserName   |URL|
      |Delphine   |https://jsonplaceholder.typicode.com/posts|

  @SearchComment
  Scenario Outline: For each post, fetch the comments and validate if the emails in the comment section are in the proper format
    Then I execute the get api with url "<URL>"
    Then I validate the comments and validate the emails is correct or not
    Examples:
      |URL|
      |https://jsonplaceholder.typicode.com/comments|

  @SearchCommentforparticularID
  Scenario Outline: For each post, fetch the comments and validate if the emails for particular id
    Then I execute the get api with url "<URL>"
    Then I validate the comments and validate the emails for particular id
    Examples:
      |URL|
      |https://jsonplaceholder.typicode.com/comments|